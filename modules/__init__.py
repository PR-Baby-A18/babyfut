from Babyfut.modules.authquick   import AuthQuickModule
from Babyfut.modules.authleague  import AuthLeagueModule
from Babyfut.modules.game        import GameModule
from Babyfut.modules.endgame     import EndGameModule
from Babyfut.modules.menu        import MenuModule
from Babyfut.modules.options     import OptionsModule
from Babyfut.modules.leaderboard import LeaderboardModule
from Babyfut.modules.privacy     import PrivacyModule
