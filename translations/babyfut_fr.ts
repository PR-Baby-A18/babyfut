<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../ui/delete_dialog_ui.py" line="70"/>
        <source>Dialog</source>
        <translation>Supression</translation>
    </message>
    <message>
        <location filename="../ui/consent_dialog_ui.py" line="39"/>
        <source>Consent Approval Needed</source>
        <translation>Validation de Consentement</translation>
    </message>
    <message>
        <location filename="../ui/delete_dialog_ui.py" line="71"/>
        <source>Changing {}&apos;s profile</source>
        <translation>Modification du profile de {}</translation>
    </message>
    <message>
        <location filename="../ui/delete_dialog_ui.py" line="72"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Select an option&lt;br/&gt;then validate by passing your badge on the table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Choisissez une option&lt;br/&gt;et validez en passant votre carte sur la table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui/delete_dialog_ui.py" line="73"/>
        <source>Delete all records</source>
        <translation>Supprimer toutes les données</translation>
    </message>
    <message>
        <location filename="../ui/delete_dialog_ui.py" line="74"/>
        <source>Remove the Picture</source>
        <translation>Suprrimer la photo</translation>
    </message>
    <message>
        <location filename="../ui/delete_dialog_ui.py" line="75"/>
        <source>Make the accout private</source>
        <translation>Rendre privé</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../ui/privacy_ui.py" line="52"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../ui/authquick_ui.py" line="222"/>
        <source>Identify yourselves! </source>
        <translation>Identifiez vous ! </translation>
    </message>
    <message>
        <location filename="../ui/authquick_ui.py" line="223"/>
        <source>Use the table&apos;s RFID reader with your card.</source>
        <translation>Utilisez les lecteurs RFID de la table avec votre carte étu.</translation>
    </message>
    <message>
        <location filename="../ui/playerlist_ui.py" line="95"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ui/authleague_ui.py" line="138"/>
        <source>Stat</source>
        <translation>Stat</translation>
    </message>
    <message>
        <location filename="../ui/endgame_ui.py" line="92"/>
        <source>Player 1</source>
        <translation>Joueur 1</translation>
    </message>
    <message>
        <location filename="../ui/endgame_ui.py" line="93"/>
        <source>Player 2</source>
        <translation>Joueur 2</translation>
    </message>
    <message>
        <location filename="../ui/authquick_ui.py" line="226"/>
        <source>vs.</source>
        <translation>vs .</translation>
    </message>
    <message>
        <location filename="../ui/authquick_ui.py" line="227"/>
        <source>Player 3</source>
        <translation>Joueur 3</translation>
    </message>
    <message>
        <location filename="../ui/authquick_ui.py" line="228"/>
        <source>Player 4</source>
        <translation>Joueur 4</translation>
    </message>
    <message>
        <location filename="../ui/endgame_ui.py" line="91"/>
        <source>Congratulations!</source>
        <translation>Félicitations !</translation>
    </message>
    <message>
        <location filename="../ui/game_ui.py" line="79"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../ui/menu_ui.py" line="137"/>
        <source>Leaderboard</source>
        <translation>Classements</translation>
    </message>
    <message>
        <location filename="../ui/leaderboard_ui.py" line="109"/>
        <source>Sort By</source>
        <translation>Trier Par</translation>
    </message>
    <message>
        <location filename="../ui/leaderboard_ui.py" line="111"/>
        <source>Victories</source>
        <translation>Victoires</translation>
    </message>
    <message>
        <location filename="../ui/leaderboard_ui.py" line="112"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../ui/leaderboard_ui.py" line="113"/>
        <source>Games Played</source>
        <translation>Parties Jouées</translation>
    </message>
    <message>
        <location filename="../ui/leaderboard_ui.py" line="114"/>
        <source>Time Played</source>
        <translation>Temps Joué</translation>
    </message>
    <message>
        <location filename="../ui/leaderboard_ui.py" line="115"/>
        <source>Jump to</source>
        <translation>Sauter à</translation>
    </message>
    <message>
        <location filename="../ui/menu_ui.py" line="134"/>
        <source>Babyf&apos; UT</source>
        <translation>Babyf&apos;UT</translation>
    </message>
    <message>
        <location filename="../ui/menu_ui.py" line="135"/>
        <source>Start Quick Game</source>
        <translation>Partie Rapide</translation>
    </message>
    <message>
        <location filename="../ui/menu_ui.py" line="136"/>
        <source>Start League Mode</source>
        <translation>Championnat</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="219"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="220"/>
        <source>Game Over Condition</source>
        <translation>Condition de Fin de Jeu</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="221"/>
        <source>By Score</source>
        <translation>Par Score</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="222"/>
        <source>By Time</source>
        <translation>Par Temps</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="223"/>
        <source>{} minutes</source>
        <translation>{} minutes</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="224"/>
        <source>League - Number of players per side</source>
        <translation>Championnat - Nombre de joueurs par coté</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="225"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="226"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="227"/>
        <source>Language</source>
        <translation>Langage</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="228"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../ui/playerlist_ui.py" line="96"/>
        <source>Surname</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../ui/privacy_ui.py" line="53"/>
        <source>Privacy</source>
        <translation>Vie Privée</translation>
    </message>
    <message>
        <location filename="../modules/authleague.py" line="51"/>
        <source>{} Victories</source>
        <translation>{} Victoires</translation>
    </message>
    <message>
        <location filename="../modules/authleague.py" line="52"/>
        <source>{} Games Played</source>
        <translation>{} Parties Jouées</translation>
    </message>
    <message>
        <location filename="../modules/authleague.py" line="53"/>
        <source>{} Goals Scored</source>
        <translation>{} Buts Marqués</translation>
    </message>
    <message>
        <location filename="../ui/playerlist_ui.py" line="97"/>
        <source>{} Victories</source>
        <comment>Form</comment>
        <translation>{} Victoires</translation>
    </message>
    <message>
        <location filename="../ui/playerlist_ui.py" line="98"/>
        <source>{} Goals Scored</source>
        <comment>Form</comment>
        <translation>{} Buts Marqués</translation>
    </message>
    <message>
        <location filename="../ui/playerlist_ui.py" line="99"/>
        <source>{} Games Played</source>
        <comment>Form</comment>
        <translation>{} Parties Jouées</translation>
    </message>
    <message>
        <location filename="../ui/playerlist_ui.py" line="100"/>
        <source>{} Minutes Played</source>
        <comment>Form</comment>
        <translation>{} Minutes Jouées</translation>
    </message>
    <message>
        <location filename="../ui/options_ui.py" line="229"/>
        <source>Fran&#xe7;ais</source>
        <translation type="obsolete">Français</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/options_ui.py" line="229"/>
        <source>Français</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/main_ui.py" line="83"/>
        <source>Babyfoot</source>
        <translation>Babyf&apos;UT</translation>
    </message>
</context>
<context>
    <name>consent</name>
    <message>
        <location filename="../modules/privacy.py" line="18"/>
        <source>&lt;p&gt;
<byte value="x9"/><byte value="x9"/>This software uses personnal information in accordance to GDPR, such as:
<byte value="x9"/><byte value="x9"/>&lt;ul&gt;
<byte value="x9"/><byte value="x9"/><byte value="x9"/>&lt;li&gt;Your Name and Surname&lt;/li&gt;
<byte value="x9"/><byte value="x9"/><byte value="x9"/>&lt;li&gt;Your Picture (if public)&lt;/li&gt;
<byte value="x9"/><byte value="x9"/><byte value="x9"/>&lt;li&gt;...&lt;/li&gt;
<byte value="x9"/><byte value="x9"/>&lt;/ul&gt;
<byte value="x9"/><byte value="x9"/>&lt;/p&gt;

<byte value="x9"/><byte value="x9"/>&lt;p&gt;
<byte value="x9"/><byte value="x9"/>That way players can keep track of their score and compare it with others.
<byte value="x9"/><byte value="x9"/>&lt;br/&gt;
<byte value="x9"/><byte value="x9"/>yada yada
<byte value="x9"/><byte value="x9"/>&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
